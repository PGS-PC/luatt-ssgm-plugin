LuaTT is a plugin created by sla.ro (Thanks jnz for original LuaPlugin and jonwil for various help with the plugin and ExEric3 for server testing and hosting) for SSGM TT (C&C Renegade server). To use this plugin, you must install Tiberian Technologies into your C&C Renegade Server (FDS - Free Dedicated Server).

This plugin enables Lua programming language for your server.

LuaTT v3 uses LuaJIT (Lua 5.1) and SQLite. Is compatible with LuaPlugin from SSGM 3.4.1, some functions might not work, read the documentation.
